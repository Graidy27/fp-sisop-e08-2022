#include <sys/stat.h>   //Buat mkdir
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dirent.h>	//Buat DIR *
#define PORT 8080

char roa[2048] = {0}; //rest of argument
char root[4] = "0";

void removeChar(char * str, char charToRemmove){
    int i, j;
    int len = strlen(str);
    for(i=0; i<len; i++)
    {
        if(str[i] == charToRemmove)
        {
            for(j=i; j<len; j++)
            {
                str[j] = str[j+1];
            }
            len--;
            i--;
        }
    }
}
//check roa
int check_roa(int sock){
	//Is there a semicolon?
	char *semicol = strstr(roa, ";");
	if(!semicol) {
		//no semicol
		write(sock, "\0", 1024);
		printf("ERROR: EXPECTED ';' AT THE END OF INPUT\n");
		return 0;
	}

	if(strlen(semicol) > 1){
		//THERE IS A Argument AFTER ';'
		char i = 1;
		while(i < strlen(semicol)) {
			//is it a bunch of ' '?
			if(semicol[i] != ' ' && semicol[i] != '\n'){
				//it is not
				write(sock, "\0", 1024);
				char *un_used = &semicol[i];
				un_used[strlen(un_used) - 1] = '\0';
				printf("ERROR: UN-USED ARGUMENT '%s'\n", un_used);
				return 0;
			}
			i++;
		}
	}
	semicol[0] = '\0';

	if(roa[strlen(roa) - 1] == ' ') {
		printf("ERROR: SEMICOLON ';' MUST NOT SEPARATED WITH THE LAST ARGUMENT\n");
		write(sock, "\0", 1024);
		return 0;
	}
	return 1;
}

//ERROR HANDLING
////UPPERCASE, LOWERCASE, AND NUMBER ONLY
int upper(char c){if( c >= 'A' && c <= 'Z') return 1; return 0;}
int lower(char c){if( c >= 'a' && c <= 'z') return 1; return 0;}
int numbr(char c){if( c >= '0' && c <= '9') return 1; return 0;}
int check_unic_char(int sock, char *temp){
	int i = 0;
	for(i = 0; i < strlen(temp); i++){
		if( !upper(temp[i]) && !lower(temp[i]) && !numbr(temp[i]) ){
			printf("ERROR: WRONG FORMAT '%s'\nERROR DETAIL: PLEASE ONLY USE UPPERCASE, LOWERCASE, OR NUMBER\n", temp);
			write(sock, "\0", 1024);
			return 1;
		}
	}
	return 0;
}

////WORD BY WORD
int next_arg(int sock, char *argument, int *i){
	int j = 0;
	char temp[1024] = {0};
	if((*i) + 1 >= strlen(roa)){
		printf("ERROR: TOO FEW ARGUMEN\n");
		write(sock, "\0", 1024);
		return 0;
	}
	(*i)++;
	while((*i) < strlen(roa) && roa[(*i)] != ' '){
		temp[j] = roa[(*i)];
		j++;
		(*i)++;
	}
	if(!strlen(temp)) {
		printf("ERROR: TOO MANY SPACES\n");
		printf("ERROR DETAIL: EACH ARGUMENT MUST SEPARATED WITH 1 SPACES\n");
		write(sock, "\0", 1024);
		return 0;
	}
	strcpy(argument, temp);
	return 1;
}

////NOT RECOGNIZED
void not_recog(int sock, char *arg){printf("ERROR: COMMAND '%s' NOT RECOGNIZED\n", arg); write(sock, "\0", 1024);}

////TOO MUCH ARGUMENT
int too_much_arg(int sock, int bookmark){
	if(bookmark < strlen(roa)) {
		char *un_used = &roa[bookmark];
		printf("ERROR: TOO MUCH ARGUMENT\n");
		printf("ERROR DETAIL: UN-USED ARGUMENT '%s'\n", un_used);
		write(sock, "\0", 1024);
		return 1;
	}
	return 0;
}





//LOGIN
int login(int sock, char username[1024], char password[1024]){
//	printf("username: %s\nPassword: %s\n", username, password);
	write(sock, username, 1024);
	write(sock, password, 1024);

	char status[200] = {0};
	read(sock, status, 200);
	printf("%s\n", status);
	if(!strcmp(status, "ACCESS DENIED")) return 0;
	return 1;
}

//ROOT GRANT  PERMISSION
void root_grant_permit(int sock){
	char arg[64] = {0}, db_name[1024] = {0}, username[1024] = {0};

	int bookmark = 0;
	//check arg
	if(!next_arg(sock, arg, &bookmark)) return;
	if(strcmp(arg, "PERMISSION")) {not_recog(sock, arg); return;}

	//check database name;
	if(!next_arg(sock, db_name, &bookmark)) return;
	if(check_unic_char(sock, db_name)) return;
	write(sock, db_name, 1024);

	//check arg
	if(!next_arg(sock, arg, &bookmark)) return;
	if(strcmp(arg, "INTO")) {not_recog(sock, arg); return;}

	//get user name
	if(!next_arg(sock, username, &bookmark)) return;
	if(check_unic_char(sock, username)) return;

	//too much argument?
	if(too_much_arg(sock, bookmark)) return;

	//If client not root
	if(root[0] == '0') {
		printf("ERROR: AUTHORITY NOT ENOUGH. MUST LOGIN VIA ROOT USER\n");
		write(sock, "\0", 1024);
		return;
	}

	//If client is root
	write(sock, username, 1024);

	char db_avail[64] = {0}, user_avail[64] = {0}, permit_granted[64] = {0};
	//db avail?
	read(sock, db_avail, 64);
	if(!strlen(db_avail)) {printf("ERROR: DATABASE NOT FOUND\n"); return;}
	else printf("%s\n", db_avail);

	//user avail?
	read(sock, user_avail, 64);
	if(!strlen(user_avail)) {printf("ERROR: USERNAME NOT FOUND\n"); return;}
	else printf("%s\n", user_avail);

	//permission granted?
	read(sock, permit_granted, 64);
	if(!strlen(permit_granted)) {printf("ERROR: USER '%s' ALREADY HAVE PERMISSION TO OPEN DB '%s'\n", username, db_name); return;}
	else printf("%s\n", permit_granted);
}

//USE
void use(int sock){
	int bookmark = 0;
	char db_name[1024] = {0};
	if(!next_arg(sock, db_name, &bookmark)) return;
	if(check_unic_char(sock, db_name)) return;
	if(too_much_arg(sock, bookmark)) return;
	write(sock, db_name, 1024);

	//check error
	char error[64] = {0};
	read(sock, error, 64);

	if(strlen(error)) {
		printf("%s\n", error);
		return;
	}
	printf("DATABASE ACCESSED\n");
}

//CREATE
void create(int sock){
	char mode[64] = {0};
	int bookmark = 0;
	if(!next_arg(sock, mode, &bookmark)) return;
	write(sock, mode, 64);
	//USER
	if(!strcmp(mode, "USER") && !strcmp(root, "1")) {
		char new_user[1024] = {0}, new_pwd[1024] = {0}, temp[32] = {0};

		//get username
		if(!next_arg(sock, new_user, &bookmark)) return;
		if(check_unic_char(sock, new_user)) return;
		write(sock, new_user, 1024);

		//check arg
		if(!next_arg(sock, temp, &bookmark)) return;
		if(strcmp(temp, "IDENTIFIED")) {
			printf("new user: %s\n", new_user); not_recog(sock, temp);
			return;
		}

		//check arg
		if(!next_arg(sock, temp, &bookmark)) return;
		if(strcmp(temp, "BY")) {not_recog(sock, temp); return;}

		//get password
		if(!next_arg(sock, new_pwd, &bookmark)) return;
		if(check_unic_char(sock, new_pwd)) return;
		if(too_much_arg(sock, bookmark)) return;

		write(sock, new_pwd, 1024);

		char success[4] = {0};
		read(sock, success, 4);
		if(success[0] == '1') printf("USER CREATED\n");
		else printf("ERROR: USERNAME EXIST\n");
	}

	//DATABASE
	else if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0};
		//get directory name
		if(!next_arg(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		if(too_much_arg(sock, bookmark)) return;
		write(sock, dir_name, 1024);

		char success[4] = {0};
		read(sock, success, 4);
		if(success[0] == '1') printf("DATABASE CREATED\n");
		else printf("ERROR: NAME EXIST\n");
	}

	//TABLE
	else if(!strcmp(mode, "TABLE")) {
        char dir_name[512] = {0};
        char* util;
        removeChar(roa, '(');
        removeChar(roa, ')');
        removeChar(roa, ',');
        if(!next_arg(sock, dir_name, &bookmark)) return;
        if(check_unic_char(sock, dir_name)) return;
        write(sock, dir_name, 1024);
        if(!next_arg(sock, dir_name, &bookmark)) return;
        util = strstr(roa, dir_name);
        write(sock, util, strlen(util));
        char success[4] = {0};
		read(sock, success, 4);
        if(success[0] == '1') printf("TABLE CREATED\n");
		else if(success[0] == '3') printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		else if(success[0] == '2') printf("ERROR: UNDEFINED DATATYPE\n");
		else printf("ERROR: TABLE EXIST\n");

	}
        else printf("ERROR: COMMAND '%s' IS NOT RECOGNIZED\n", mode);
}

//DROP
void drop(int sock){
    char mode[512] = {0};
	int bookmark = 0;
	if(!next_arg(sock, mode, &bookmark)) return;
	if(check_unic_char(sock, mode)) return;
	write(sock, mode, 1024);
	//USER
	if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0};
		//get directory name
		if(!next_arg(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		if(too_much_arg(sock, bookmark)) return;
		write(sock, dir_name, 1024);
		char success[4] = {0};
		read(sock, success, 4);
		if(success[0] == '1') printf("DATABASE DELETED\n");
		else if(success[0] == '0') printf("ERROR: DATABASE DO NOT EXIST\n");
		else printf("ERROR : YOU DO NOT HAVE ACCESS!!\n");
		return;
	}
	else if(!strcmp(mode, "TABLE")) {
		char dir_name[512] = {0};
		if(!next_arg(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		if(too_much_arg(sock, bookmark)) return;
		write(sock, dir_name, 1024);
		char success[4] = {0};
		read(sock, success, 4);
		if(success[0] == '1') printf("TABLE DELETED\n");
		else if(success[0] == '0') printf("ERROR: TABLE DO NOT EXIST\n");
		else if(success[0] == '3') printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		else printf("TABLE IS NOT REMOVED\n");

	}
	else if(!strcmp(mode, "COLUMN")){
		char dir_name[512] = {0};
		if(!next_arg(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		printf("%s-a\n", dir_name);
		write(sock, dir_name, 1024);
		if(!next_arg(sock, dir_name, &bookmark)) return;
		printf("%s-a\n", dir_name);
		if(strcmp(dir_name, "FROM")){
			printf("WRONG SYNTAX!\n");
			return;
		}
		if(!next_arg(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		printf("%s-a\n", dir_name);
		write(sock, dir_name, 1024);
		char success[4] = {0};
		read(sock, success, 4);
		if(success[0] == '0') printf("TABLE DOESN'T EXIST\n");
		else if(success[0] == '1') printf("SUCCESSFULLY DELETED\n");
		else printf("COLUMN DOESN'T EXIST\n");
	}
	else printf("'%s' NOT RECOGNIZED. DO YOU MEAN 'USER', 'DATABASE' OR 'TABLE'?\n", mode);
}

//INSERT
void db_insert(int sock){
	char mode[512] = {0};
	char success[4] = {0};
	char avail[4] = {0};
	char* val;
	int bookmark = 0;
	if(!next_arg(sock, mode, &bookmark)) return;
	if(check_unic_char(sock, mode)) return;
	printf("%s\n", mode);
	write(sock, mode, 512);
	if(!strcmp(mode, "INTO")){
		char dir_name[1024];
		read(sock, avail, 4);
		printf("nih sudah read success %c\n", success[0]);
		if(avail[0] == '1'){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			return;
		}
		if(!next_arg(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		write(sock, dir_name, 1024);
		if(!next_arg(sock, dir_name, &bookmark)) return;
		printf("%s %s\n", roa, dir_name);
		removeChar(roa, ',');
		removeChar(roa, '(');
		removeChar(roa, ')');
		removeChar(roa, 39);
		removeChar(dir_name, '(');
		removeChar(dir_name, ',');
		removeChar(dir_name, 39);
		printf("%s %s\n", roa, dir_name);
		val = strstr(roa, dir_name);
		printf("%s\n", val);
		write(sock, val, 1024);
		read(sock, success, 4);
		if(success[0] == '0') printf("TABLE DOESN'T EXIST\n");
		else printf("SUCCESSFULLY INSERTED\n");
	}
	else printf("'%s' NOT RECOGNIZED. DO YOU MEAN 'INTO'?\n", mode);
}

//WHERE
void where_sug(){
	printf("SUGGESTION 1: WHERE [column_name]='value' FOR STRING\n");
	printf("EXAMPLE 1   : WHERE name='AYAKA'\n");
	printf("SUGGESTION 2: WHERE [column_name]=value FOR INT\n");
	printf("EXAMPLE 2   : WHERE age=19\n\n");
}
///format -> column1:valu1:datatype
int check_where(int sock, char *where_arg){
	char *value = strstr(where_arg, "=");
	if(!value) {
		printf("ERROR: UNKNOWN FORMAT\n"); where_sug();
		write(sock, "\0", 1024);
		return 0;
	}
	char temp[1024] = {0}, temp2[1024] = {0};

	//separate
	strcpy(temp2, value); //column value
	value[0] = '\0';
	strcpy(temp, where_arg); //column name

	if(check_unic_char(sock, temp)) return 0; //check column name

	//cek apakah value int atau string
	char c = '\'', dataType[16] = {0};
	if(temp2[1] == c && temp2[strlen(temp2) - 1] == c){ //check string
		temp2[strlen(temp2) - 1] = '\0';
		char *temp3 = &temp2[2];
		if(check_unic_char(sock, temp3)) {
			where_sug();
			return 0;
		}
		strcpy(temp2, temp3);
		strcpy(dataType, "string");
	}

	else{	//check int
		for(int i = 1; i < strlen(temp2); i++){
			if(!numbr(temp2[i])) {
				printf("ERROR: UNRECOGNIZE FORMAT\n"); where_sug();
				write(sock, "\0", 1024);
				return 0;
			}
		}
		char *temp3 = &temp2[1];
		strcpy(temp2, temp3);
		strcpy(dataType, "int");
	}

	strcpy(where_arg, temp);
	strcat(where_arg, ":");
	strcat(where_arg, temp2);
	strcat(where_arg, ":");
	strcat(where_arg, dataType);
	return 1;
}
int is_where (int sock, int *bookmark){
	char arg[1024] = {0};
	if((*bookmark) < strlen(roa)) {
		write(sock, "WHERE", 1024);	//tell server there is a chance for where command
		if(!next_arg(sock, arg, bookmark)) return 0;
		if(strcmp(arg, "WHERE")) {
			not_recog(sock, arg);
			printf("DO YOU MEAN 'WHERE'?\n");
			write(sock, "\0", 1024);
			return 0;
		}
		write(sock, arg, 1024);
		if(!next_arg(sock, arg, bookmark)) return 0;
		if(too_much_arg(sock, (*bookmark))) return 0;
		if(!check_where(sock, arg)) return 0;
	}
	write(sock, arg, 1024);
	return 1;
}

//SELECT
void sel(int sock){
	int bookmark = 0, loop = 1, i = 1;
	char arg[1024] = {0}, table_name[1024] = {0}, header[2048] = {0};
	while(loop){
		if(!next_arg(sock, arg, &bookmark)) return;

		//select star
		if(i == 1 && !strcmp(arg, "*")) {
			write(sock, arg, 1024);
			break;
		}

		if(arg[strlen(arg) - 1] != ',') loop = 0;
		else arg[strlen(arg) - 1] = '\0';

		//check select star
		if(check_unic_char(sock, arg)) return;

		//make header (Buat judul kalo perintah sukses dieksekusi)
		strcat(header, arg); strcat(header, ":");

		//send to server
		write(sock, arg, 1024);
	}
	//sending random character to tell server to stop looping
	write(sock, "-", 1024);
	if(!next_arg(sock, arg, &bookmark)) return;
	if(strcmp(arg, "FROM")) {not_recog(sock, arg); return;}

	if(!next_arg(sock, table_name, &bookmark)) return;
	if(check_unic_char(sock, table_name)) return;
	write(sock, table_name, 1024);

	//check where
	if(!is_where(sock, &bookmark)) return;


	//if there is error, print the error
	char status[256] = {0};
	read(sock, status, 256);	//have the user input "use" command
	if(strlen(status)) {printf("%s\n", status); return;}

	read(sock, status, 256);	//table availability
	if(strlen(status)) {printf("%s\n", status); return;}

	read(sock, status, 256);	//column availability
	while(strlen(status)){
		printf("ERROR: COLUMN '%s' NOT RECOGNIZED\n", status);
		read(sock, status, 256);
		if(!strlen(status)) return;
	}


	//display the data
	loop = 1; char data[2048];

	//header dipake disini
	header[strlen(header) - 1] = '\0';
	printf("%s\n", header);
	while(loop){
		read(sock, data, 2048);
		printf("%s\n", data);
		if(!strlen(data)) break;
	}
}


//UPDATE
void update(int sock){}

//DELETE
void del(int sock){}

int main(int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	printf("Request a connection to server...");
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
	else {
		puts("Connection established\n");
		puts("You will access the server when no one is currently accessing it\nPlease wait...\n");
		sleep(3);
		char permission[4] = {0};
		read(sock, permission, 4);

		//check parameter input
		strcpy(root, "0");
		if(argc < 2) strcpy(root, "1"); //If it is root
		write(sock, root, 4);
		//end of check parameter input

		char username[1024] = {0}, password[1024] = {0};
		strcpy(username, argv[2]);
		strcpy(password, argv[4]);
		if(!strcmp(root, "1") || login(sock, username, password) == 1) {
			printf("Welcome!\nType 'exit;' to exit the program\n");
			while(true){
				char com[1024] = {0};
				scanf("%s", com);
				fgets(roa, 2048, stdin);

				if(com[ strlen(com)-1 ] != ';' && !check_roa(sock)) continue;
				if(com[ strlen(com)-1 ] == ';') com[ strlen(com)-1 ] = '\0';
				write(sock, com, 1024);


				if(!strcmp(com, "CREATE")) create(sock);
				else if(!strcmp(com, "USE")) use(sock);
				else if(!strcmp(com, "GRANT") && !strcmp(root, "1")) root_grant_permit(sock);
				else if(!strcmp(com, "DROP")) drop(sock);
				else if(!strcmp(com, "INSERT")) db_insert(sock);
				else if(!strcmp(com, "UPDATE")) update(sock);
				else if(!strcmp(com, "DELETE")) del(sock);
				else if(!strcmp(com, "SELECT")) sel(sock);
				else if(!strcmp(com, "exit")) {printf("See you later!\n"); break;}
				else {printf("ERROR: COMMAND '%s' NOT RECOGNIZED\n", com); continue;}
			}
		}
		else printf("See you later!\n");
	}


//	send(sock , hello , strlen(hello) , 0 );
//	printf("Hello message sent\n");
//	valread = read( sock , buffer, 1024);
//	printf("%s\n",buffer );
	return 0;
}

